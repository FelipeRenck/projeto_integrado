#include "TimerOne.h>

#define PRESSAO_MAX 180
#define PRESSAO_MIN 110
#define PRESSAO_CRITICA 90
#define PRESSAO_TRABALHO_MIN 120
#define PRESSAO_TRABALHO_MAX 160

/* Struct para bomba */
struct bomba
{
  bool ligada;
  bool desligada;
  bool posicao_remoto;
};

/* Struct para válvula */
struct valvula
{
  bool acionada;
  bool desacionada;
}

/* Struct para alarme */
struct alarme
{
  bool pressaoBaixa;
  bool pressaoAlta;
}

/* Struct para máquina de estados */
struct estados
{
  int estadoAnterior;
  int estadoAtual;
  int estadoProximo;
}

float pressostato;

/* Função para execução do timer 1 */
void callback()
{
  
}

void setup() {
  // put your setup code here, to run once:

  /* Declaração das estruturas */
  struct bomba bomba1;
  struct bomba bomba2;
  struct bomba bombaEmergencia;

  struct valvula valvulaReposicao;

  struct alarme alarmePressao;

  struct estado maquinaEstados;

  Timer1.initialize(1000000); // configurando Timer1 para 1 segundo
  Timer1.attachInterrupt(callback);
}

void loop() {
  // put your main code here, to run repeatedly:
  
}
